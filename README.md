dnsmasq-pxe
=========

Create an iPXE server (DHCP,TFTP,HTTP) that uses dnsmasq to run DHCP in proxy mode. This allows it to run on networks that already have a DHCP server, without causing interference. Like the primary DHCP server, it will respond to DHCP requests but only with the missing opions required for network booting.

Requirements
------------

This role assumes that the target is running Ubuntu and has a static IP. 

Role Variables
--------------

See `defaults/main.yaml`

Dependencies
------------

None.

Example Playbook
----------------

    - name: install iPXE server
      hosts: raspberrypi
      pre_tasks:
      roles:
         - role: dnsmasq-pxe
           
License
-------

MIT

Author Information
------------------

[Matt Ray](https://gitlab.com/m-j-ray)
